import React from 'react'
import styles from './PriceProduct.module.scss'

const PriceProduct= (props) => (
  <tr className={styles.wrapper}>
    <td className={styles.product}>{props.product}</td>
    <td className={styles.price}>{props.price}</td>
  </tr>
);

export default PriceProduct;