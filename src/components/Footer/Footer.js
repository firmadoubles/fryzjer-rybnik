import React from 'react';
import styles from './Footer.module.scss';

const Footer = () => (
    <footer className={styles.wrapper}>
        <p>Copyright &copy; 2019-2020 <a href="https://szymonsobik.pl" target="_blank" rel="noopener noreferrer" title="tworzenie stron www Rybnik" className={styles.link}>Strony internetowe Rybnik</a></p>
    </footer>
);

export default Footer;