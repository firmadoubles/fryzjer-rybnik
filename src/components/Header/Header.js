import React from 'react';
import HeaderNavigation from './HeaderNavigation';
import styles from './Header.module.scss';
import { withRouter } from 'react-router-dom';
import CallToUs from './HeaderCta';

const Header = (props) => (
    <header className={props.location.pathname === '/' ? styles.wrapper : styles.darkWrapper}>
        <HeaderNavigation />
        <CallToUs />
    </header>
);

export default withRouter(Header);