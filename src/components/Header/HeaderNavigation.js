import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './HeaderNavigation.module.scss';

const HeaderNavigation = () => (
    <nav>
        <ul className={styles.wrapper}>
            <li className={styles.navItem}>
                <NavLink
                    exact
                    activeClassName={styles.navItemLinkActive}
                    className={styles.navItemLink}
                    to="/">strona główna</NavLink>
            </li>
            <li className={styles.navItem}>
                <NavLink
                    activeClassName={styles.navItemLinkActive}
                    className={styles.navItemLink}
                    to="/o-mnie">o mnie</NavLink>
            </li>
            <li className={styles.navItem}>
                <NavLink
                    activeClassName={styles.navItemLinkActive}
                    className={styles.navItemLink}
                    to="/oferta">oferta</NavLink>
            </li>
            <li className={styles.navItem}>
                <NavLink
                    activeClassName={styles.navItemLinkActive}
                    className={styles.navItemLink}
                    to="/galeria-fryzur">galeria fryzur</NavLink>
            </li>
            <li className={styles.navItem}>
                <NavLink
                    activeClassName={styles.navItemLinkActive}
                    className={styles.navItemLink}
                    to="/cennik">cennik</NavLink>
            </li>
            <li className={styles.navItem}>
                <NavLink
                    activeClassName={styles.navItemLinkActive}
                    className={styles.navItemLink}
                    to="/kontakt">kontakt</NavLink>
            </li>
        </ul>
    </nav>
);

export default HeaderNavigation;