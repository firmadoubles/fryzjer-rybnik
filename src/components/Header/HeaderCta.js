import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone } from '@fortawesome/free-solid-svg-icons';
import { withRouter } from 'react-router-dom';
import styles from './HeaderCta.module.scss';
import ReactGA from 'react-ga';
ReactGA.initialize('UA-5275423-10');

const showPhone = () => {
    let elem = document.getElementById("clickToShow");
    elem.style.display = 'block';
    let btn = document.getElementById("clickToShowButton");
    btn.style.display = 'none';
    ReactGA.event({
        category: 'kontakt',
        action: 'click',
        label: 'numer',
        value: 1
    });
};

const CallToUs = (props) => (
    <div
        className={props.location.pathname === '/' ? styles.wrapper : styles.wrapperDark}
    >
        {/* <p>Urlop macierzyński</p> */}
        <FontAwesomeIcon icon={faPhone} />
        <p>
            {/* <a
                href="tel:+48796711515"
                title="Zadzwoń do nas"
                onClick={showPhone}
            >796 711 515</a> */}
            <a
                href="tel:+48796711515"
                id="clickToShow"
                className={styles.clickToShow}>
                    796 711 515
            </a>
            <span
                id="clickToShowButton"
                className={styles.clickToShowButton}
                onClick={showPhone}>
                    796 XXX XXX <span className={styles.showbtn}>Pokaż</span>
            </span>
        </p>
    </div>
);

export default withRouter(CallToUs);