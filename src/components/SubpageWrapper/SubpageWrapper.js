import React from 'react'
import styles from './SubpageWrapper.module.scss'

const SubpageWrapper = (props) => (
  <div className={styles.wrapper}>
    {props.children}
  </div>
);

export default SubpageWrapper;