import React from 'react';
import Title from './../../components/Title/Title';
import SubpageWrapper from './../../components/SubpageWrapper/SubpageWrapper'
import {Helmet} from 'react-helmet'

const AboutView = () => (
    <>
        <Helmet>
            <title>O mnie - Fryzjer Mobilny Rybnik</title>
            <link rel="canonical" href="http://www.fryzjerrybnik.pl/o-mnie" />
        </Helmet>
        <SubpageWrapper>
            <Title>kilka słów o mnie</Title>
            <p>Nazywam się Małgorzata Juraszczyk.</p>
            <p>Jestem wykwalifikowaną fryzjerką z kilkunastoletnim doświadczeniem.</p>
            <p>Do swojej pracy podchodzę z pasją i zaangażowaniem.</p>
            <p>Najważniejsze jest dla mnie Twoje zdowolenie z jakości wykonanej usługi.</p>
            <p>Oferuję usługi fryzjerskie z dojazdem do Klienta. W domowym zaciszu będziesz mógł/mogła zrelaksować się, a ja profesjonalnie zadbam o Twoje włosy. Moje usługi są idealnym rozwiązaniem dla:</p>
            <ul>
                <li>
                    <p>zapracowanych pań i panów</p>
                </li>
                <li>
                    <p>zabieganych mam</p>
                </li>
                <li>
                    <p>dzieci obawiających się wizyty w salonie fryzjerskim</p>
                </li>
                <li>
                    <p>osób starszych, którym podróże sprawiają trudność</p>
                </li>
            </ul>
            <p>Zapraszam do kontaktu i skorzystania z moich usług.</p>
        </SubpageWrapper>
    </>
);

export default AboutView;