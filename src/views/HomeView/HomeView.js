import React from 'react'
import styles from './HomeView.module.scss'
import {Helmet} from 'react-helmet'

const HomeView = () => (
    <>
        <Helmet>
            <title>Fryzjer Mobilny Gosia - Rybnik, Żory, Wodzisław Śląski</title>
            <link rel="canonical" href="http://www.fryzjerrybnik.pl/" />
        </Helmet>
        <div className={styles.wrapper}>
            <div className={styles.intro}>
                <h1 className={styles.introTitle}>Twój<br />domowy<br />salon fryzjerski</h1>
                <p className={styles.introTxt}>Najwyższa jakość, profesjonalne kosmetyki, pełna gama usług.</p>
                <p className={styles.introTxt}>Rybnik, Wodzisław Śląski, Żory i okolice.</p>
            </div>
        </div>
    </>
);

export default HomeView;