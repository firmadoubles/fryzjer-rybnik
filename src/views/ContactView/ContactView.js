import React from 'react'
import Title from './../../components/Title/Title'
import SubpageWrapper from './../../components/SubpageWrapper/SubpageWrapper'
import {Helmet} from 'react-helmet'

const ContactView = () => (
    <>
        <Helmet>
            <title>Umów się na wizytę - Fryzjer Mobilny Rybnik</title>
            <link rel="canonical" href="http://www.fryzjerrybnik.pl/kontakt" />
        </Helmet>
        <SubpageWrapper>
            <Title>kontakt</Title>
            <p>W związku z obostrzeniami pandemicznymi moja działalność jest zawieszona do odwołania!</p>
        </SubpageWrapper>
    </>
);

export default ContactView;