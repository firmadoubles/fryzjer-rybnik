import React from 'react';
import './Root.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import AboutView from '../AboutView/AboutView';
import ContactView from '../ContactView/ContactView';
import HomeView from './../HomeView/HomeView';
import Header from './../../components/Header/Header';
import Footer from './../../components/Footer/Footer';
import OfferView from '../OfferView/OfferView';
import PriceListView from './../PriceListView/PriceListView';
import GalleryView from './../GalleryView/GalleryView'

const Root = () => (
  <BrowserRouter>
    <>
      <Header />
      <Switch>
        <Route exact path="/" component={HomeView} />
        <Route path="/o-mnie" component={AboutView} />
        <Route path="/oferta" component={OfferView} />
        <Route path="/galeria-fryzur" component={GalleryView} />
        <Route path="/cennik" component={PriceListView} />
        <Route path="/kontakt" component={ContactView} />
      </Switch>
      <Footer />
    </>
  </BrowserRouter>
);

export default Root;
