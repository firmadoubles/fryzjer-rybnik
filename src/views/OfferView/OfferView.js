import React from 'react';
import Title from './../../components/Title/Title';
import SubpageWrapper from './../../components/SubpageWrapper/SubpageWrapper'
import {Helmet} from 'react-helmet'

const OfferView = () => (
    <>
        <Helmet>
            <title>Oferta usług fryzjerski - Fryzjer Mobilny Rybnik</title>
            <link rel="canonical" href="http://www.fryzjerrybnik.pl/oferta" />
        </Helmet>
        <SubpageWrapper>
            <Title>zakres moich usług</Title>
            <p>Oferta jaką dla Państwa przygotowałam obejmuje szeroki zakres usług fryzjerskich, od strzyżeń, koloryzacji, prostowania, po czesania na specjalne okazje. Wszystko to otrzymasz nie wychodząc z domu, bez męczących wyjazdów, w godzinach i dniach, które TOBIE najbardziej odpowiadają.</p>
            <p>Fryzjer z dojazdem do domu, to idealne rozwiązanie na miło spędzony czas, z profesjonalnym efektem końcowym.</p>
            <ul>
                <li>
                    <p>Strzyżenia</p>
                </li>
                <li>
                    <p>Modelowanie</p>
                </li>
                <li>
                    <p>Upięcia</p>
                </li>
                <li>
                    <p>Koloryzacja oraz baleyage</p>
                </li>
                <li>
                    <p>Sombre, ombre</p>
                </li>
                <li>
                    <p>Cięcia męskie i dziecięcie</p>
                </li>
            </ul>
        </SubpageWrapper>
    </>
);

export default OfferView;