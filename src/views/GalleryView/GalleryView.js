import React from 'react';
import Title from './../../components/Title/Title';
import SubpageWrapper from './../../components/SubpageWrapper/SubpageWrapper'
import {Helmet} from 'react-helmet'
import ImageGallery from 'react-image-gallery'
import styles from './GalleryView.module.scss'
import "react-image-gallery/styles/css/image-gallery.css"

class GalleryView extends React.Component {
    render() {
        const images = [
            {
                original: require('../../assets/gallery/img001.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb001.jpg'),
            },
            {
                original: require('../../assets/gallery/img005.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb005.jpg'),
            },
            {
                original: require('../../assets/gallery/img007.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb007.jpg'),
            },
            {
                original: require('../../assets/gallery/img008.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb008.jpg'),
            },
            {
                original: require('../../assets/gallery/img011.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb011.jpg'),
            },
            {
                original: require('../../assets/gallery/img012.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb012.jpg'),
            },
            {
                original: require('../../assets/gallery/img015.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb015.jpg'),
            },
            {
                original: require('../../assets/gallery/img018.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb018.jpg'),
            },
            {
                original: require('../../assets/gallery/img022.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb022.jpg'),
            },
            {
                original: require('../../assets/gallery/img024.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb024.jpg'),
            },
            {
                original: require('../../assets/gallery/img027.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb027.jpg'),
            },
            {
                original: require('../../assets/gallery/img033.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb033.jpg'),
            },
            {
                original: require('../../assets/gallery/img034.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb034.jpg'),
            },
            {
                original: require('../../assets/gallery/img039.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb039.jpg'),
            },
            {
                original: require('../../assets/gallery/img046.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb046.jpg'),
            },
            {
                original: require('../../assets/gallery/img048.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb048.jpg'),
            },
            {
                original: require('../../assets/gallery/img050.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb050.jpg'),
            },
            {
                original: require('../../assets/gallery/img056.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb056.jpg'),
            },
            {
                original: require('../../assets/gallery/img059.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb059.jpg'),
            },
            {
                original: require('../../assets/gallery/img063.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb063.jpg'),
            },
            {
                original: require('../../assets/gallery/img066.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb066.jpg'),
            },
            {
                original: require('../../assets/gallery/img069.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb069.jpg'),
            },
            {
                original: require('../../assets/gallery/img075.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb075.jpg'),
            },
            {
                original: require('../../assets/gallery/img080.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb080.jpg'),
            },
            {
                original: require('../../assets/gallery/img083.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb083.jpg'),
            },
            {
                original: require('../../assets/gallery/img086.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb086.jpg'),
            },
            {
                original: require('../../assets/gallery/img088.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb088.jpg'),
            },
            {
                original: require('../../assets/gallery/img091.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb091.jpg'),
            },
            {
                original: require('../../assets/gallery/img098.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb098.jpg'),
            },
            {
                original: require('../../assets/gallery/img099.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb099.jpg'),
            },
            {
                original: require('../../assets/gallery/img102.jpg'),
                thumbnail: require('../../assets/gallery/thumbs/thumb102.jpg'),
            },
        ]

        return (
            <>
                <Helmet>
                    <title>Galeria fryzur - Fryzjer Mobilny Rybnik</title>
                    <link rel="canonical" href="http://www.fryzjerrybnik.pl/o-mnie" />
                </Helmet>
                <SubpageWrapper>
                    <Title>galeria fryzur</Title>
                    <div className={styles.galleryWrapper}>
                        <ImageGallery
                            items={images}
                            useBrowserFullscreen={false}
                            showFullscreenButton={false}
                        />
                    </div>
                </SubpageWrapper>
            </>
        );
    }
};

export default GalleryView;