import React from 'react'
import Title from './../../components/Title/Title'
import SubpageWrapper from './../../components/SubpageWrapper/SubpageWrapper'
import {Helmet} from 'react-helmet'
import styles from './PriceListView.module.scss'
import PriceProduct from '../../components/PriceProduct/PriceProduct'

const PriceListView = () => (
    <>
        <Helmet>
            <title>Cennik usług fryzjerski - Fryzjer Mobilny Rybnik</title>
            <link rel="canonical" href="http://www.fryzjerrybnik.pl/cennik" />
        </Helmet>
        <SubpageWrapper>
            <Title>cennik</Title>
            <p>Zapoznaj się z cennikiem naszych usług</p>
            <table className={styles.priceTable}>
                <thead>
                    <tr>
                        <th className={styles.priceTitle}>Usługa fryzjerska</th>
                        <th className={styles.priceTitle}>Cena</th>
                    </tr>
                </thead>
                <tbody>
                    <PriceProduct
                        product="Strzyżenie damskie"
                        price="40 - 55zł"
                    />
                    <PriceProduct
                        product="Strzyżenie męskie"
                        price="40 zł"
                    />
                    <PriceProduct
                        product="Strzyżenie dziecięce"
                        price="20zł do 7 lat"
                    />
                    <PriceProduct
                        product="Modelowanie + strzyżenie"
                        price="90 - 120zł"
                    />
                    <PriceProduct
                        product="Prostowanie"
                        price="35 - 45zł"
                    />
                    <PriceProduct
                        product="Upięcia"
                        price="60 - 90zł"
                    />
                    <PriceProduct
                        product="Loki/fale"
                        price="80 - 100zł"
                    />
                    <PriceProduct
                        product="Fryzura ślubna próbna"
                        price="100zł"
                    />
                    <PriceProduct
                        product="Fryzura ślubna"
                        price="150zł"
                    />
                    <PriceProduct
                        product="Farbowanie 1 kolor"
                        price="100 - 200zł"
                    />
                    <PriceProduct
                        product="Pasemka"
                        price="120 - 200zł"
                    />
                    <PriceProduct
                        product="Pasemka sama korona"
                        price="120 - 140zł"
                    />
                    <PriceProduct
                        product="Ombre/sombre"
                        price="150 - 230zł"
                    />
                    <PriceProduct
                        product="Dekoloryzacja + farbowanie + nanoplex"
                        price="175 - 300zł"
                    />
                    <PriceProduct
                        product="Nanoplex"
                        price="80zł"
                    />
                    <PriceProduct
                        product="Depilacja brwi"
                        price="20zł"
                    />
                    <PriceProduct
                        product="Wąsik woskiem"
                        price="20zł"
                    />
                    <PriceProduct
                        product="Henna brwi + depilacja"
                        price="25zł"
                    />
                    <PriceProduct
                        product="Henna rzęs"
                        price="20zł"
                    />
                </tbody>
            </table>
            <p>Dojazd:<br />
            do 20km - gratis<br />
            20 - 50km - 20 - 30zł<br />
            powyżej 50km - do ustalenia<br />
            Przy kwocie usługi powyżej 200zł do 50km - dojazd gratis</p>
            <p>W weekendy i dni wolne - minimalny koszt usługi to 100zł.</p>
        </SubpageWrapper>
    </>
);

export default PriceListView;